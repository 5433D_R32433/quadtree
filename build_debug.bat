@echo off

if not defined DevEnvDir (
call vc x64
)

set CFLAGS= -MTd -Z7 -GS- -DPLATFORM_DESKTOP -Oi -Gm- -GR- -sdl- -FC -IC:..\src -IC:..\src\glfw\include
set LFLAGS= -link /SUBSYSTEM:CONSOLE -out:gp.exe
set LIBS= 
set WIN32_LIBS= kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib 

set SOURCES= ../main.c ../src/rglfw.c ../src/rmodels.c ../src/rshapes.c ../src/rtext.c ../src/rtextures.c ../src/utils.c ../src/rcore.c ../src/raudio.c   

if exist debug (
rmdir /S /Q debug
mkdir debug
pushd debug
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug
) else (
mkdir debug
pushd debug
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug
)



