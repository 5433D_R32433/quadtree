#include <stdint.h>
#include <math.h>
#include "raylib.h"

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

#define SCREEN_WIDTH  700
#define SCREEN_HEIGHT 700


#define RCOLOR(c) ( Color )             \
{                                       \
    ( uint8_t ) ( ( c >> 24 ) & 0xFF ), \
    ( uint8_t ) ( ( c >> 16 ) & 0xFF ), \
    ( uint8_t ) ( ( c >>  8 ) & 0xFF ), \
    ( uint8_t ) ( ( c >>  0 ) & 0xFF )  \
}                                       

typedef struct point_t
{
    int32_t x, y;
} point_t;


point_t point_create ( int x, int y )
{
    point_t p = { 0 };
    p.x = x;
    p.y = y;
    return p;
}

typedef struct aabb_t
{
    int32_t x;
    int32_t y;
    
    int32_t w;
    int32_t h;
    
} aabb_t;


typedef struct quadtree_t
{
    aabb_t   bounds;
    int32_t  capacity;
    point_t  *points;
    bool     divided;
    int32_t  point_count;
    
    
    struct quadtree_t *ne;
    struct quadtree_t *nw;
    struct quadtree_t *se;
    struct quadtree_t *sw;
    
} quadtree_t;


quadtree_t *quadtree_create ( int32_t x, 
                              int32_t y, 
                              int32_t w, 
                              int32_t h, 
                              int32_t capacity )
{
    quadtree_t  q     = { 0 };
    quadtree_t *qtree = 0;
    
    q.bounds.x = x;
    q.bounds.y = y;
    q.bounds.w = w;
    q.bounds.h = h;
    q.capacity = capacity;
    q.divided  = false;
    
    arrpush ( qtree, q );
    
    return qtree;
}


void quadtree_subdivide ( quadtree_t *qtree )
{
    int32_t x = qtree->bounds.x;
    int32_t y = qtree->bounds.y;
    int32_t w = qtree->bounds.w;
    int32_t h = qtree->bounds.h;
    
    qtree->nw = quadtree_create ( x, y,                 w / 2 + 1, h / 2 + 1, qtree->capacity );    
    qtree->ne = quadtree_create ( x + w / 2, y,         w / 2, h / 2 + 1,     qtree->capacity );
    qtree->sw = quadtree_create ( x, y + h / 2,         w / 2 + 1, h / 2,     qtree->capacity );
    qtree->se = quadtree_create ( x + w / 2, y + h / 2, w / 2, h / 2,         qtree->capacity );

    qtree->divided = true;
}


bool quadtree_contains ( quadtree_t *qtree, point_t p )
{
    return ( p.x > ( qtree->bounds.x )                   &&
             p.x < ( qtree->bounds.x + qtree->bounds.w ) &&
             p.y > ( qtree->bounds.y )                   &&
             p.y < ( qtree->bounds.y + qtree->bounds.h ) );
             
}

static int32_t point_count = 0;

void quadtree_insert ( quadtree_t *qtree, point_t p )
{
    if ( !quadtree_contains ( qtree, p ) )
    {
        return;
    }
    
    if ( arrlen ( qtree->points ) < qtree->capacity )
    {
        arrpush ( qtree->points, p );
        ++point_count;
    }
    else
    {
        if ( !qtree->divided )
        {
            quadtree_subdivide ( qtree );
        }
        
        quadtree_insert ( qtree->nw, p );
        quadtree_insert ( qtree->ne, p );
        quadtree_insert ( qtree->sw, p );
        quadtree_insert ( qtree->se, p );
    }
}


void random_point_create ( quadtree_t *qtree, 
                           int32_t     count, 
                           int32_t     w, 
                           int32_t     h )
{
    for ( int i = 0; i < count; i++ )
    {
        point_t p = { 0 };
        p.x = ( rand ( ) % ( w - 50 + 1 ) ) + 50;
        p.y = ( rand ( ) % ( h - 50 + 1 ) ) + 50;
        
        quadtree_insert ( qtree, p );
    }
}


void quadtree_draw ( quadtree_t *qtree )
{
    
    DrawRectangleLines ( qtree->bounds.x, 
                         qtree->bounds.y, 
                         qtree->bounds.w, 
                         qtree->bounds.h, 
                         RCOLOR ( 0x665139FF ) );
    
    if ( qtree->divided )
    {
        quadtree_draw ( qtree->nw );
        quadtree_draw ( qtree->ne );
        quadtree_draw ( qtree->sw );
        quadtree_draw ( qtree->se );
    }
    
    for ( int32_t i = 0; i < arrlen ( qtree->points ); i++ )
    {
        
        DrawCircle ( qtree->points [ i ].x, 
                     qtree->points [ i ].y, 
                     2.0f,
                     RCOLOR ( 0x9E3216FF ) );
    }
    
}


void quadtree_destroy ( quadtree_t *qtree ) 
{
    arrfree ( qtree->points );
    arrfree ( qtree->nw );
    arrfree ( qtree->ne );
    arrfree ( qtree->sw );
    arrfree ( qtree->se );
    arrfree ( qtree );
}


void create_point_by_mouse ( quadtree_t *qtree )
{
    if ( IsMouseButtonPressed ( MOUSE_LEFT_BUTTON ) )
    {
        Vector2 v = GetMousePosition ( );
        point_t p = { 0 };
        p.x = v.x;
        p.y = v.y;
        quadtree_insert ( qtree, p );
    }
}


int main() 
{
    InitWindow ( SCREEN_WIDTH, SCREEN_HEIGHT, "Quad Tree" );
    // FLAG_WINDOW_UNDECORATED 
    SetWindowState ( FLAG_WINDOW_RESIZABLE | FLAG_WINDOW_HIGHDPI );
    // SetTargetFPS(60);
    
    
    quadtree_t *qtree = 0;
    qtree = quadtree_create ( 50, 50, 600 ,600, 4 );
    // random_point_create ( qtree, 1000, 600, 600 );
    
    
    while ( !WindowShouldClose ( ) ) 
    {
        BeginDrawing    ( );
        ClearBackground ( RCOLOR ( 0x141414FF ) );
        
        create_point_by_mouse ( qtree );
        quadtree_draw ( qtree );
        
        char buf [ 16 ] = { 0 };
        snprintf ( buf, sizeof ( buf ), "%d\n", point_count );
        DrawText ( buf, 30, 30, 16, RCOLOR ( 0X1E7197FF ) );  
        
        
        
        DrawFPS    ( 10, 10 );
        EndDrawing ( );
    }
    
    
    quadtree_destroy ( qtree );
    CloseWindow();
    return 0;
}

